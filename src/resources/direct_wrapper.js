export default function directWrapper() {
  return `
      <div 
        ggnoclick 
        id="mGGUID" 
        style="_CLEARCSS_; width: 318px; height: 478px; cursor:pointer;">
        <style>
          [C1_STYLES]
          [C2_STYLES]
        </style>
        <div class="container" onclick="openAd()"></div>
        [CONTAINER_2]
        <iframe
            width="318"
            height="478"
            src="[CREATIVE_SOURCE]"
            style="width: 318px; height: 478px"
            frameborder="0"
            allowfullscreen
        ></iframe>
        <script>
          var G = window.GUMGUM || parent.GUMGUM || top.GUMGUM;
          function openAd() {
            GUMGUMAD.openAd();
          }
        </script>
        [MOAT_PIXEL]
      </div>
    `;
}
