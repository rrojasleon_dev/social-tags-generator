export default function MOATPIX() {
  return `
        <script>
          var G = window.GUMGUM || parent.GUMGUM || top.GUMGUM;
          try {
            G.loadObj(
              "https://z.moatads.com/gumgumdv360display244359675132/moatad.js#moatClientLevel1=${"${INSERTION_ORDER_ID}"}&moatClientLevel2=${"${CAMPAIGN_ID}"}&moatClientLevel3=${"${PUBLISHER_ID}"}&moatClientLevel4=${"${CREATIVE_ID}"}",
              {
                parent: G["GGUID"].element || G["GGUID"].el,
                type: "script",
              }
            );
          } catch (e) {
            console.log("GUMGUM", e);
          }
        </script>`;
}
