import creativeProfile from "../models/creativeModel";
import * as XLSX from "xlsx";

class utils {
  parseCSV(csvFile) {
    const importedCreatives = [];
    const fileReader = new FileReader();
    return new Promise((resolve, reject) => {
      fileReader.onload = (e) => {
        const fileData = e.target.result;
        const fileToArray = fileData.split("\r");
        if (
          fileToArray[0] !== 'name,source,type'
        ) {
          reject();
        }
        fileToArray.forEach((record, index) => {
          if (index > 0) {
            const recordArray = record.split(",");
            const creative = new creativeProfile(recordArray);
            importedCreatives.push(creative);
          }
        });
      };
      fileReader.onloadend = () => {
        resolve(importedCreatives);
      };

      fileReader.readAsText(csvFile);
    });
  }

  parseExcel(excelFile) {
    const importedCreatives = [];
    const reader = new FileReader();
    return new Promise((resolve) => {
      reader.onload = (evt) => {
        // evt = on_file_select event
        /* Parse data */
        const bstr = evt.target.result;
        const wb = XLSX.read(bstr, { type: "binary" });
        /* Get first worksheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        /* Convert array of arrays */
        const data = XLSX.utils.sheet_to_json(ws);

        data.forEach(record => {
          const creative = new creativeProfile();
          creative.name = record.name;
          creative.source = record.source;
          creative.type = record.type;
          importedCreatives.push(creative);
        });
        resolve(importedCreatives);
      };
      reader.readAsBinaryString(excelFile);
    });
  }

  convertToJson(fileContent) {
    const creativesList = [];
    const fileData = fileContent.replace("\n", "");
    const fileToArray = fileData.split("\r");
    fileToArray.forEach((record, index) => {
      if (index > 0) {
        const recordArray = record.split(",");
        const creative = new creativeProfile(recordArray);
        creativesList.push(creative);
      }
    });
    return creativesList;
  }
}

export default new utils();
