class creativeProfile {
    constructor(data = null) {
        if (data !== null) {
            this.name = data[0].replace(/^\s*[\r\n]/gm, "");
            this.source = data[1];
            this.type = data[2];
        }
    }
    name = "";
    source = "";
    type = "native";
    isProgrammatic = false;
    useMOAT = false;
    hasSound = false;
    sound = {
        containers: {
            c1: {
              top: 0,
              height: 50
            },
            c2: {
              top: 60,
              height: 50
            }
          }
    };
    sizing = {
        width: 320,
        height: 480
    };
    programmatic = {
        dspMacro: "",
        clickTracker: "",
        extraImpressions: false,
        extraPixels: [],
    };
}

export default creativeProfile;